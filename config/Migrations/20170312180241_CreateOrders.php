<?php

use Migrations\AbstractMigration;

class CreateOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('orders');
        $table->addColumn('order_number', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ])
            ->addIndex('order_number', ['unique' => true]);
        $table->addColumn('price', 'integer', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('client_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])
            ->addIndex('client_id')
            ->addForeignKey('client_id', 'clients');
        $table->addColumn('status_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])
            ->addIndex('status_id')
            ->addForeignKey('status_id', 'statuses');
        $table->addColumn('priority_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])
            ->addIndex('priority_id')
            ->addForeignKey('priority_id', 'priorities');
        $table->addColumn('description', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('order_data', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('order_description', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
