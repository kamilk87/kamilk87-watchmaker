<?php

use Migrations\AbstractMigration;

class CreateItems extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('items');
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('item_number', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ])->addIndex('item_number', ['unique' => true]);
        $table->addColumn('price', 'integer', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('amount', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('alarm', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('brand_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])
            ->addIndex('brand_id')
            ->addForeignKey('brand_id', 'brands');
        $table->addColumn('item_attributes', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
