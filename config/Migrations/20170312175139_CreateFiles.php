<?php

use Migrations\AbstractMigration;

class CreateFiles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('files');

        $table->addColumn('order_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addIndex('order_id')
            ->addForeignKey('order_id', 'orders');
        $table->addColumn('path', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ])
            ->addIndex('name', ['unique' => true]);

        $table->addColumn('dir', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);


        $table->create();
    }
}
