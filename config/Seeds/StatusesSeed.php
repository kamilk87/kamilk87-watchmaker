<?php
use Migrations\AbstractSeed;

/**
 * Statuses seed.
 */
class StatusesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'nowe',
                'created' => '2017-07-08 12:28:00',
                'modified' => '2017-07-08 12:28:00',
            ],
            [
                'id' => '2',
                'name' => 'oczekujące',
                'created' => '2017-07-08 12:28:18',
                'modified' => '2017-07-08 12:28:18',
            ],
            [
                'id' => '3',
                'name' => 'w trakcie realizacji',
                'created' => '2017-07-08 12:28:34',
                'modified' => '2017-07-08 12:28:34',
            ],
            [
                'id' => '4',
                'name' => 'wykonane',
                'created' => '2017-07-08 12:29:04',
                'modified' => '2017-07-08 12:29:04',
            ],
            [
                'id' => '5',
                'name' => 'zamkniete',
                'created' => '2017-07-08 12:29:19',
                'modified' => '2017-07-08 12:29:19',
            ],
        ];

        $table = $this->table('statuses');
        $table->insert($data)->save();
    }
}
