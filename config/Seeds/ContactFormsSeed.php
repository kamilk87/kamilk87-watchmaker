<?php
use Migrations\AbstractSeed;

/**
 * ContactForms seed.
 */
class ContactFormsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'telefon',
                'created' => '2017-07-08 12:30:20',
                'modified' => '2017-07-08 12:30:20',
            ],
            [
                'id' => '2',
                'name' => 'email',
                'created' => '2017-07-08 12:30:35',
                'modified' => '2017-07-08 12:30:35',
            ],
        ];

        $table = $this->table('contact_forms');
        $table->insert($data)->save();
    }
}
