<?php
use Migrations\AbstractSeed;

/**
 * Priorities seed.
 */
class PrioritiesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'bardzo niski',
                'created' => '2017-07-08 12:24:55',
                'modified' => '2017-07-08 12:24:55',
            ],
            [
                'id' => '2',
                'name' => 'niski',
                'created' => '2017-07-08 12:25:16',
                'modified' => '2017-07-08 12:25:16',
            ],
            [
                'id' => '3',
                'name' => 'normalny',
                'created' => '2017-07-08 12:25:37',
                'modified' => '2017-07-08 12:25:37',
            ],
            [
                'id' => '4',
                'name' => 'wysoki',
                'created' => '2017-07-08 12:25:53',
                'modified' => '2017-07-08 12:25:53',
            ],
            [
                'id' => '5',
                'name' => 'bardzo wysoki',
                'created' => '2017-07-08 12:26:15',
                'modified' => '2017-07-08 12:26:15',
            ],
        ];

        $table = $this->table('priorities');
        $table->insert($data)->save();
    }
}
