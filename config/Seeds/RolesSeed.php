<?php
use Migrations\AbstractSeed;

/**
 * Roles seed.
 */
class RolesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'admin',
                'role_description' => 'role with the highest priviledges',
                'created' => '2017-07-08 12:21:45',
                'modified' => '2017-07-08 12:21:45',
            ],
            [
                'id' => '2',
                'name' => 'user',
                'role_description' => 'base role for normal user - in app meaning client',
                'created' => '2017-07-08 12:22:35',
                'modified' => '2017-07-08 12:22:35',
            ],
        ];

        $table = $this->table('roles');
        $table->insert($data)->save();
    }
}
