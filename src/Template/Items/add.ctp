<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista Przedmiotów'), ['action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista Marek'), ['controller' => 'Brands', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowa Marka'), ['controller' => 'Brands', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="items form content">
            <?= $this->Form->create($item, ['class' => 'form']) ?>
            <fieldset>
                <legend><?= _('Dodawanie Przedmiotów') ?></legend>
                <?php
                echo $this->Form->control('name', [
                    'label' => _('Nazwa'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('item_number', [
                    'label' => _('Numer przedmiotu'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('price', [
                    'type' => 'text',
                    'label' => _('Cena'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('amount', [
                    'label' => _('Ilość'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('alarm', [
                    'label' => _('Ilość alarmowa'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('brand_id', ['options' => $brands,
                    'label' => _('Marka'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('item_attributes', [
                    'label' => _('Szczegóły'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                ?>
            </fieldset>
            <?= $this->Form->button(_('Zapisz'), ['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
