<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Form->postLink(
                                _('Kasowanie'),
                                ['action' => 'delete', $item->id],
                                ['confirm' => __('Czy napewno usunąć przedmiot # {0}?', $item->id)]
                            )
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Edycja Przedmiotu'), ['action' => 'edit', $item->id]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista Przedmiotów'), ['action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy Przedmiot'), ['action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista Marek'), ['controller' => 'Brands', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowa Marka'), ['controller' => 'Brands', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="items view large-9 medium-8 columns content">
            <h3><?= _('Przedmiot nr: ').h($item->id) ?></h3>
            <table class="table table-responsive table-bordered">
                <tr>
                    <th scope="row"><?= __('Nazwa') ?></th>
                    <td><?= h($item->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Numer Przedmiotu') ?></th>
                    <td><?= h($item->item_number) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Marka') ?></th>
                    <td><?= $item->has('brand') ? $this->Html->link($item->brand->name, ['controller' => 'Brands', 'action' => 'view', $item->brand->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Cena') ?></th>
                    <td><?= $this->Number->format(number_format($item->price/100,2)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Ilość') ?></th>
                    <td><?= $this->Number->format($item->amount) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Ilość alrmowa') ?></th>
                    <td><?= $this->Number->format($item->alarm) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Data utworzenia') ?></th>
                    <td><?= h($item->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Data ostatniej modyfikacji') ?></th>
                    <td><?= h($item->modified) ?></td>
                </tr>
            </table>
            <div class="row">
                <h4><?= _('Szczegóły') ?></h4>
                <?= $this->Text->autoParagraph(h($item->item_attributes)); ?>
            </div>
        </div>
    </div>
</div>
