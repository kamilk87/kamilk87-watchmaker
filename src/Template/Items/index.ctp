<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy Przedmiot'), ['action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista Marek'), ['controller' => 'Brands', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowa Marka'), ['controller' => 'Brands', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="items index content">
            <h3><?= _('Przedmioty') ?></h3>
            <table class="table table-responsive table-bordered table-striped">
                <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id', _('Numer')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name', _('Nazwa')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('item_number', _('Numer seryjny')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('price', _('Cena')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('amount', _('Ilość na stanie')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('alarm', _('Ilość alarmowa')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('brand_id', _('Marka')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created', _('Data utworzenia')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('modified', _('Data ostatniej modyfikacji')) ?></th>
                    <th scope="col" class="actions"><?= _('Akcje') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($items as $item): ?>
                    <tr>
                        <td><?= $this->Number->format($item->id) ?></td>
                        <td><?= h($item->name) ?></td>
                        <td><?= h($item->item_number) ?></td>
                        <td><?= $this->Number->format(number_format($item->price,2)) ?></td>
                        <td><?= $this->Number->format($item->amount) ?></td>
                        <td><?= $this->Number->format($item->alarm) ?></td>
                        <td><?= $item->has('brand') ? $this->Html->link($item->brand->name, ['controller' => 'Brands', 'action' => 'view', $item->brand->id]) : '' ?></td>
                        <td><?= h($item->created) ?></td>
                        <td><?= h($item->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(_('Podgląd'), ['action' => 'view', $item->id]) ?>
                            <?= $this->Html->link(_('Edycja'), ['action' => 'edit', $item->id]) ?>
                            <?= $this->Form->postLink(_('Kasowanie'), ['action' => 'delete', $item->id], ['confirm' => __('Czy napewno usunąć zlecenie # {0}?', $item->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . _('pierwsza')) ?>
                    <?= $this->Paginator->prev('< ' . _('poprzednia')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(_('następna') . ' >') ?>
                    <?= $this->Paginator->last(_('ostatnia') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => _('Strona {{page}} z {{pages}}, Pokazuje {{current}} wyników z {{count}} wszystkich')]) ?></p>
            </div>
        </div>
    </div>
</div>
