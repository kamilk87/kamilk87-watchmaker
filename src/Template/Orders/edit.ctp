<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Podgląd zlecenia'), ['action' => 'view', $order->id]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Form->postLink(_('Kasowanie lecenia'), ['action' => 'delete', $order->id], ['confirm' => __('Czy napewno usunąć zlecenie # {0}?', $order->id)]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Powrót do listy zleceń'), ['action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowe zlecenie'), ['action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista klientów'), ['controller' => 'Clients', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy klient'), ['controller' => 'Clients', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="orders form content">
            <?= $this->Form->create($order, [
                'class' => 'form',
                'type' => 'file'
            ]) ?>
            <fieldset>
                <legend><?= _('Edycja zleceń') ?></legend>
                <?php
                echo $this->Form->control('order_number', [
                    'label' => _('Numer zlecenia'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('price', [
                    'type' => 'text',
                    'label' => _('Cena'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('client_id', ['options' => $clients,
                    'label' => _('Klient'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('status_id', ['options' => $statuses,
                    'label' => _('Status'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('priority_id', ['options' => $priorities,
                    'label' => _('Prioritet'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('description', [
                    'label' => _('Uwagi'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('order_data', [
                    'label' => _('Dane dodatkowe'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('order_description', [
                    'label' => _('Opis zlecenia'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->file('files[]', [
                    'multiple' => true,
                    'label' => _('Zdjęcia'),
                    'class' => 'form-control',
                ]);
                ?>
            </fieldset>
            <?= $this->Form->button(_('Zapisz'), ['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
