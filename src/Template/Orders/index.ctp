<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowe zlecenie'), ['action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista klientów'), ['controller' => 'Clients', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy klient'), ['controller' => 'Clients', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="orders index content">
            <h3><?= _('Zlecenia') ?></h3>
            <table class="table table-responsive table-bordered table-striped">
                <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id', _('Numer')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('order_number', _('Numer zlecenia')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('price', _('Wartość')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('client_id', _('Klient')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('status_id', _('Status')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('priority_id', _('Priorytet')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created', _('Data utworzenia')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('modified', _('Data ostatniej modyfikacji')) ?></th>
                    <th scope="col" class="actions"><?= _('Akcje') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($orders as $order): ?>
                    <tr>
                        <td><?= $this->Number->format($order->id) ?></td>
                        <td><?= h($order->order_number) ?></td>
                        <td><?= $this->Number->format(number_format($order->price/100,2)) ?></td>
                        <td><?= $order->has('client') ? $this->Html->link($order->client->name, ['controller' => 'Clients', 'action' => 'view', $order->client->id]) : '' ?></td>
                        <td><?= $order->has('status') ? $this->Html->link($order->status->name, ['controller' => 'Statuses', 'action' => 'view', $order->status->id]) : '' ?></td>
                        <td><?= $order->has('priority') ? $this->Html->link($order->priority->name, ['controller' => 'Priorities', 'action' => 'view', $order->priority->id]) : '' ?></td>
                        <td><?= h($order->created) ?></td>
                        <td><?= h($order->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(_('Podgląd'), ['action' => 'view', $order->id]) ?>
                            <?= $this->Html->link(_('Edycja'), ['action' => 'edit', $order->id]) ?>
                            <?= $this->Form->postLink(_('Kasowanie zlecenia'), ['action' => 'delete', $order->id], ['confirm' => __('Czy napewno usunąć zlecenie # {0}?', $order->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . _('pierwsza')) ?>
                    <?= $this->Paginator->prev('< ' . _('poprzednia')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(_('następna') . ' >') ?>
                    <?= $this->Paginator->last(_('ostatnia') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => _('Strona {{page}} z {{pages}}, Pokazuje {{current}} wyników z {{count}} wszystkich')]) ?></p>
            </div>
        </div>
    </div>
</div>
