<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Edycja zlecenia'), ['action' => 'edit', $order->id]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Form->postLink(_('Kasowanie zlecenia'), ['action' => 'delete', $order->id], ['confirm' => __('Czy napewno usunąć zlecenie # {0}?', $order->id)]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Powrót do listy zleceń'), ['action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowe zlecenie'), ['action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista klientów'), ['controller' => 'Clients', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy klient'), ['controller' => 'Clients', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="orders view content">
            <h3><?= _('Zlecenie nr: ') . h($order->id) ?></h3>
            <table class="table table-responsive table-bordered">
                <tr>
                    <th scope="row"><?= __('Numer zamówienia') ?></th>
                    <td><?= h($order->order_number) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Klient') ?></th>
                    <td><?= $order->has('client') ? $this->Html->link($order->client->name, ['controller' => 'Clients', 'action' => 'view', $order->client->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Status') ?></th>
                    <td><?= $order->has('status') ? $this->Html->link($order->status->name, ['controller' => 'Statuses', 'action' => 'view', $order->status->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Priorytet') ?></th>
                    <td><?= $order->has('priority') ? $this->Html->link($order->priority->name, ['controller' => 'Priorities', 'action' => 'view', $order->priority->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Cena') ?></th>
                    <td><?= $this->Number->format(number_format($order->price / 100,2)) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Data utworzenia') ?></th>
                    <td><?= h($order->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Data ostatniej modyfikacji') ?></th>
                    <td><?= h($order->modified) ?></td>
                </tr>
            </table>
            <div class="row">
                <h4><?= _('Uwagi') ?></h4>
                <?= $this->Text->autoParagraph(h($order->description)); ?>
            </div>
            <div class="row">
                <h4><?= _('Dodatkowe dane') ?></h4>
                <?= $this->Text->autoParagraph(h($order->order_data)); ?>
            </div>
            <div class="row">
                <h4><?= _('Opis zlecenia') ?></h4>
                <?= $this->Text->autoParagraph(h($order->order_description)); ?>
            </div>
            <div class="row">
                <h4><?= _('Dołączone pliki') ?></h4>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 order-files-container">
                    <?php if ($order->files): ?>
                        <?php foreach ($order->files as $oneFile): ?>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 image-container">
                                <div class="panel panel-primary">
                                    <figure>
                                        <figcaption>
                                            <button type="button"
                                                    data-id="<?= $oneFile->id ?> "
                                                    class="close delete-file btn-danger btn"
                                                    aria-label="<?= _('Usuń') ?>"><span
                                                    aria-hidden="true">&times;</span>
                                            </button>
                                        </figcaption>
                                        <?php echo $this->Html->image('/files/files/name/' . $oneFile->dir . DS . 'square_' . $oneFile->name, ['class' => 'img-responsive']); ?>
                                    </figure>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.order-files-container').on('click', '.delete-file', function () {
            var _$self = $(this);
            var fileId = _$self.data('id');
            $.post('<?= $this->Url->build([
                    'controller' => 'Files',
                    'action' => 'delete',
                    '_ext' => 'json'
                ]);?>',
                {
                    'id': fileId,
                    '_Token': '<?= $this->request->_csrfToken?>',
                    '_Token.fields': ['files']
                },
                function (data) {
                    console.log(data);
                    _$self.parents('.image-container').remove();
                }
            )
                .fail(function () {
                    console.log('Nie udało sie skasować pliku')
                });
        })
    });
</script>
