<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Powrót do listy'), ['action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy Klient'), ['action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Edycja Klienta'), ['action' => 'edit', $client->id]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Form->postLink(_('Usunięcie Klienta'), ['action' => 'delete', $client->id], ['confirm' => __('Czy napewno usunąć klienta # {0}?', $client->id)]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowe zamówienie'), ['controller' => 'Orders', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <?= $this->Html->link(_('Lista zamówień'), ['controller' => 'Orders', 'action' => 'index']) ?>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="clients view content">
            <h3><?= _('Klient nr: ') . h($client->id) ?></h3>
            <table class="table table-responsive table-bordered">
                <tr>
                    <th scope="row"><?= _('Imię') ?></th>
                    <td><?= h($client->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Nazwisko') ?></th>
                    <td><?= h($client->surname) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Telefon') ?></th>
                    <td><?= h($client->phone) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Email') ?></th>
                    <td><?= h($client->email) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Preferowana forma kontaktu') ?></th>
                    <td><?= $client->has('contact_form') ? $this->Html->link($client->contact_form->name, ['controller' => 'ContactForms', 'action' => 'view', $client->contact_form->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Data utworzenia') ?></th>
                    <td><?= h($client->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Data ostatniej modyfikacji') ?></th>
                    <td><?= h($client->modified) ?></td>
                </tr>
            </table>
            <div class="row">
                <h4><?= _('Zamówienia Klienta') ?></h4>
                <?php if (!empty($client->orders)): ?>
                    <table cellpadding="0" cellspacing="0" class="table table-bordered table-responsive table-striped">
                        <tr>
                            <th scope="col"><?= _('Id') ?></th>
                            <th scope="col"><?= _('Numer zlececnia') ?></th>
                            <th scope="col"><?= _('wartość zlecenia') ?></th>
                            <th scope="col"><?= _('Status') ?></th>
                            <th scope="col"><?= _('Priority') ?></th>
                            <th scope="col"><?= _('Opis') ?></th>
                            <th scope="col"><?= _('Dodatkowe dane') ?></th>
                            <th scope="col"><?= _('Opis zlecenią') ?></th>
                            <th scope="col"><?= _('Data utworzenia') ?></th>
                            <th scope="col"><?= _('Data ostatniej modyfikacji') ?></th>
                            <th scope="col" class="actions"><?= _('Akcje') ?></th>
                        </tr>
                        <?php foreach ($client->orders as $orders): ?>
                            <tr>
                                <td><?= h($orders->id) ?></td>
                                <td><?= h($orders->order_number) ?></td>
                                <td><?= h(number_format($orders->price/100,2)) ?></td>
                                <td><?= h($orders->status_id) ?></td>
                                <td><?= h($orders->priority_id) ?></td>
                                <td><?= h($orders->description) ?></td>
                                <td><?= h($orders->order_data) ?></td>
                                <td><?= h($orders->order_description) ?></td>
                                <td><?= h($orders->created) ?></td>
                                <td><?= h($orders->modified) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(_('Podgląd'), ['controller' => 'Orders', 'action' => 'view', $orders->id]) ?>
                                    <?= $this->Html->link(__('Edycja'), ['controller' => 'Orders', 'action' => 'edit', $orders->id]) ?>
                                    <?= $this->Form->postLink(__('Usuń'), ['controller' => 'Orders', 'action' => 'delete', $orders->id], ['confirm' => __('Czy jesteś pewien że chcesz usunąć zlecenie   # {0}?', $orders->id)]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
