<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Powrót do listy'), ['action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy Klient'), ['action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Form->postLink(_('Usunięcie Klienta'), ['action' => 'delete', $client->id], ['confirm' => __('Czy napewno usunąć klienta # {0}?', $client->id)]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowe zamówienie'), ['controller' => 'Orders', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <?= $this->Html->link(_('Lista zamówień'), ['controller' => 'Orders', 'action' => 'index']) ?>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="clients form content">
            <?= $this->Form->create($client, ['class' => 'form']) ?>
            <fieldset>
                <legend><?= _('Edycja klienta') ?></legend>
                <?php
                echo $this->Form->control('name', [
                    'label' => _('Imię'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('surname', [
                    'label' => _('Nazwisko'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('phone', [
                    'label' => _('Telefon'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('email', [
                    'label' => _('Email'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                echo $this->Form->control('contact_form_id', ['options' => $contactForms,
                    'label' => _('Preferowana forma kontaktu'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                ?>
            </fieldset>
            <?= $this->Form->button(_('Zapisz'), ['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
