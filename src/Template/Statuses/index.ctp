<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy Status'), ['action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Powrót do listy zleceń'), ['controller' => 'Orders', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowe zlecenie'), ['controller' => 'Orders', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista klientów'), ['controller' => 'Clients', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy klient'), ['controller' => 'Clients', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="statuses index content">
            <h3><?= _('Statusy') ?></h3>
            <table class="table table-responsive table-bordered table-striped">
                <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id', _('Numer')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name', _('Nazwa')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created', _('Data utworzenia')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('modified', _('Data ostatniej modyfikacji')) ?></th>
                    <th scope="col" class="actions"><?= _('Akcje') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($statuses as $status): ?>
                    <tr>
                        <td><?= $this->Number->format($status->id) ?></td>
                        <td><?= h($status->name) ?></td>
                        <td><?= h($status->created) ?></td>
                        <td><?= h($status->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(_('Podgląd'), ['action' => 'view', $status->id]) ?>
                            <?= $this->Html->link(_('Edycja'), ['action' => 'edit', $status->id]) ?>
                            <?= $this->Form->postLink(_('Kasowanie zlecenia'), ['action' => 'delete', $status->id], ['confirm' => __('Czy napewno usunąć zlecenie # {0}?', $status->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . _('pierwsza')) ?>
                    <?= $this->Paginator->prev('< ' . _('poprzednia')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(_('następna') . ' >') ?>
                    <?= $this->Paginator->last(_('ostatnia') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => _('Strona {{page}} z {{pages}}, Pokazuje {{current}} wyników z {{count}} wszystkich')]) ?></p>
            </div>
        </div>
    </div>
</div>
