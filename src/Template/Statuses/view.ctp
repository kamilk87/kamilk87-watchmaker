<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Edycja statusu'), ['action' => 'edit', $status->id]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Form->postLink(_('Kasowanie statusu'), ['action' => 'delete', $status->id], ['confirm' => __('Czy napewno usunąć status # {0}?', $status->id)]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Powrót do listy statusów'), ['action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy Status'), ['action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista klientów'), ['controller' => 'Clients', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy klient'), ['controller' => 'Clients', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(__('Lista zleceń'), ['controller' => 'Orders', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(__('Nowe Zelecenie'), ['controller' => 'Orders', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="statuses view content">
            <h3><?= h($status->name) ?></h3>
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= _('Nazwa') ?></th>
                    <td><?= h($status->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Numer') ?></th>
                    <td><?= $this->Number->format($status->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Data utworzenia') ?></th>
                    <td><?= h($status->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Data ostatniej modyfikacji') ?></th>
                    <td><?= h($status->modified) ?></td>
                </tr>
            </table>
            <div class="row">
                <h4><?= _('Powiązane zlecenia') ?></h4>
                <?php if (!empty($status->orders)): ?>
                    <table class="table table-responsive table-bordered table-striped">
                        <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id', _('Numer')) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('order_number', _('Numer zlecenia')) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('price', _('Wartość')) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('client_id', _('Klient')) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('status_id', _('Status')) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('priority_id', _('Priorytet')) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('created', _('Data utworzenia')) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('modified', _('Data ostatniej modyfikacji')) ?></th>
                            <th scope="col" class="actions"><?= _('Akcje') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($status->orders as $order): ?>
                            <tr>
                                <td><?= $this->Number->format($order->id) ?></td>
                                <td><?= h($order->order_number) ?></td>
                                <td><?= $this->Number->format(number_format($order->price/100,2)) ?></td>
                                <td><?= $order->has('client') ? $this->Html->link($order->client->name, ['controller' => 'Clients', 'action' => 'view', $order->client->id]) : '' ?></td>
                                <td><?= $order->has('status') ? $this->Html->link($order->status->name, ['controller' => 'Statuses', 'action' => 'view', $order->status->id]) : '' ?></td>
                                <td><?= $order->has('priority') ? $this->Html->link($order->priority->name, ['controller' => 'Priorities', 'action' => 'view', $order->priority->id]) : '' ?></td>
                                <td><?= h($order->created) ?></td>
                                <td><?= h($order->modified) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(_('Podgląd'), ['action' => 'view', $order->id]) ?>
                                    <?= $this->Html->link(_('Edycja'), ['action' => 'edit', $order->id]) ?>
                                    <?= $this->Form->postLink(_('Kasowanie zlecenia'), ['action' => 'delete', $order->id], ['confirm' => __('Czy napewno usunąć zlecenie # {0}?', $order->id)]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
