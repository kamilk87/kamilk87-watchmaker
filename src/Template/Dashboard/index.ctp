<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3><?= _('Zrealizowane zlecenia w : ') . date('Y/m'); ?></h3>
            </div>
            <div class="panel-body">
                <?php if ($lastMonthRealizedOrders): ?>
                    <table class="table table-responsive table-bordered table-striped">
                        <thead>
                        <tr>
                            <th scope="col"><?= _('Numer') ?></th>
                            <th scope="col"><?= _('Numer zlecenia') ?></th>
                            <th scope="col"><?= _('Wartość') ?></th>
                            <th scope="col"><?= _('Klient') ?></th>
                            <th scope="col"><?= _('Status') ?></th>
                            <th scope="col"><?= _('Priorytet') ?></th>
                            <th scope="col"><?= _('Data utworzenia') ?></th>
                            <th scope="col"><?= _('Data ostatniej modyfikacji'); ?></th>
                            <th scope="col" class="actions"><?= _('Akcje') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($lastMonthRealizedOrders as $order): ?>
                            <tr>
                                <td><?= $this->Number->format($order->id) ?></td>
                                <td><?= h($order->order_number) ?></td>
                                <td><?= $this->Number->format(number_format($order->price/100,2)) ?> PLN</td>
                                <td>
                                    <?= $order->has('client') ? $this->Html->link($order->client->name, ['controller' => 'Clients', 'action' => 'view', $order->client->id]) : '' ?></td>
                                <td><?= $order->has('status') ? $this->Html->link($order->status->name, ['controller' => 'Statuses', 'action' => 'view', $order->status->id]) : '' ?></td>
                                <td><?= $order->has('priority') ? $this->Html->link($order->priority->name, ['controller' => 'Priorities', 'action' => 'view', $order->priority->id]) : '' ?></td>
                                <td><?= h($order->created) ?></td>
                                <td><?= h($order->modified) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(_('Podgląd'), ['controller' => 'Orders', 'action' => 'view', $order->id]) ?>
                                    <?= $this->Html->link(_('Edycja'), ['controller' => 'Orders', 'action' => 'edit', $order->id]) ?>
                                    <?= $this->Form->postLink(_('Kasowanie zlecenia'), ['controller' => 'Orders', 'action' => 'delete', $order->id], ['confirm' => __('Czy napewno usunąć zlecenie # {0}?', $order->id)]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>
            <div class="panel-footer"></div>
        </div>
    </div>


</div>
<div class="row">
    <div class="col-lg-12 col-md-12cu col-sm-12 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2><?= _('Ostatnio dodane zlecenia'); ?></h2>
            </div>
            <div class="panel-body">
                <?php if ($lastOrders): ?>
                    <table class="table table-responsive table-bordered table-striped">
                        <thead>
                        <tr>
                            <th scope="col"><?= _('Numer') ?></th>
                            <th scope="col"><?= _('Numer zlecenia') ?></th>
                            <th scope="col"><?= _('Wartość') ?></th>
                            <th scope="col"><?= _('Klient') ?></th>
                            <th scope="col"><?= _('Status') ?></th>
                            <th scope="col"><?= _('Priorytet') ?></th>
                            <th scope="col"><?= _('Data utworzenia') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('modified', _('Data ostatniej modyfikacji')) ?></th>
                            <th scope="col" class="actions"><?= _('Akcje') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($lastOrders as $order): ?>
                            <tr>
                                <td><?= $this->Number->format($order->id) ?></td>
                                <td><?= h($order->order_number) ?></td>
                                <td><?= $this->Number->format(number_format($order->price/100,2)); ?> PLN</td>
                                <td><?= $order->has('client') ? $this->Html->link($order->client->name, ['controller' => 'Clients', 'action' => 'view', $order->client->id]) : '' ?></td>
                                <td><?= $order->has('status') ? $this->Html->link($order->status->name, ['controller' => 'Statuses', 'action' => 'view', $order->status->id]) : '' ?></td>
                                <td><?= $order->has('priority') ? $this->Html->link($order->priority->name, ['controller' => 'Priorities', 'action' => 'view', $order->priority->id]) : '' ?></td>
                                <td><?= h($order->created) ?></td>
                                <td><?= h($order->modified) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(_('Podgląd'), ['controller' => 'Orders', 'action' => 'view', $order->id]) ?>
                                    <?= $this->Html->link(_('Edycja'), ['controller' => 'Orders', 'action' => 'edit', $order->id]) ?>
                                    <?= $this->Form->postLink(_('Kasowanie zlecenia'), ['controller' => 'Orders', 'action' => 'delete', $order->id], ['confirm' => __('Czy napewno usunąć zlecenie # {0}?', $order->id)]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>
            <div class="panel-footer">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2><?= _('Ostatnio dodani klienci'); ?></h2>
            </div>
            <div class="panel-body">
                <?php if ($lastClients): ?>
                    <table class="table table-responsive table-bordered table-striped">
                        <thead>
                        <tr>
                            <th scope="col"><?= _('Numer') ?></th>
                            <th scope="col"><?= _('Imię') ?></th>
                            <th scope="col"><?= _('Nazwisko') ?></th>
                            <th scope="col"><?= _('Telefon') ?></th>
                            <th scope="col"><?= _('Adres email') ?></th>
                            <th scope="col"><?= _('Preferowana forma kontaktu') ?></th>
                            <th scope="col"><?= _('Data utworzenia') ?></th>
                            <th scope="col" class="actions"><?= _('Akcje') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($lastClients as $client): ?>
                            <tr>
                                <td><?= $this->Number->format($client->id) ?></td>
                                <td><?= h($client->name) ?></td>
                                <td><?= h($client->surname) ?></td>
                                <td><?= h($client->phone) ?></td>
                                <td><?= h($client->email) ?></td>
                                <td><?= $client->has('contact_form') ? $this->Html->link($client->contact_form->name, ['controller' => 'ContactForms', 'action' => 'view', $client->contact_form->id]) : '' ?></td>
                                <td><?= h($client->created) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(_('Podgląd'), ['controller' => 'Clients', 'action' => 'view', $client->id]) ?>
                                    <?= $this->Html->link(_('Edycja'), ['controller' => 'Clients', 'action' => 'edit', $client->id]) ?>
                                    <?= $this->Form->postLink(_('Usuń'), ['controller' => 'Clients', 'action' => 'delete', $client->id], ['confirm' => __('Are you sure you want to delete # {0}?', $client->id)]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>
            <div class="panel-footer">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2><?= _('Ostatnio dodane przedmioty'); ?></h2>
            </div>
            <div class="panel-body">
                <?php if ($lastItems): ?>
                    <table class="table table-responsive table-bordered table-striped">
                        <thead>
                        <tr>
                            <th scope="col"><?= _('Numer') ?></th>
                            <th scope="col"><?= _('Nazwa') ?></th>
                            <th scope="col"><?= _('Numer seryjny') ?></th>
                            <th scope="col"><?= _('Cena') ?></th>
                            <th scope="col"><?= _('Ilość na stanie') ?></th>
                            <th scope="col"><?= _('Ilość alarmowa') ?></th>
                            <th scope="col"><?= _('Marka') ?></th>
                            <th scope="col"><?= _('Data utworzenia') ?></th>
                            <th scope="col"><?= _('Data ostatniej modyfikacji') ?></th>
                            <th scope="col" class="actions"><?= _('Akcje') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($lastItems as $item): ?>
                            <tr>
                                <td><?= $this->Number->format($item->id) ?></td>
                                <td><?= h($item->name) ?></td>
                                <td><?= h($item->item_number) ?></td>
                                <td><?= $this->Number->format(number_format($item->price/100,2)); ?> PLN</td>
                                <td><?= $this->Number->format($item->amount) ?></td>
                                <td><?= $this->Number->format($item->alarm) ?></td>
                                <td><?= $item->has('brand') ? $this->Html->link($item->brand->name, ['controller' => 'Brands', 'action' => 'view', $item->brand->id]) : '' ?></td>
                                <td><?= h($item->created) ?></td>
                                <td><?= h($item->modified) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(_('Podgląd'), ['controller' => 'Items', 'action' => 'view', $item->id]) ?>
                                    <?= $this->Html->link(_('Edycja'), ['controller' => 'Items', 'action' => 'edit', $item->id]) ?>
                                    <?= $this->Form->postLink(_('Kasowanie'), ['controller' => 'Items', 'action' => 'delete', $item->id], ['confirm' => __('Czy napewno usunąć zlecenie # {0}?', $item->id)]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>
            <div class="panel-footer">
            </div>
        </div>
    </div>
</div>
<?= debug($userObj); ?>
