<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Kasperiuk Salon Zegarmistrzowski - Strona główna</title>

    <base href="http://www.salon-zegarmistrzowski.pl/"/>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
    <META NAME="ROBOTS" CONTENT="INDEX,FOLLOW,ALL">
    <META HTTP-EQUIV="Content-Language" CONTENT="pl">
    <META NAME="revisit-after" CONTENT="2 days">
    <META NAME="description" CONTENT="Salon Zegarmistrzowski w Białymstoku">
    <META NAME="author" CONTENT="www.ExtremeWebsites.pl">
    <meta name="Keywords"
          content="zegarmistrz; białystok, naprawa zegarkow, wymiana, paski, bransoletki,zegarki wodoodporne,zegarki szwajcarskie,kasperiuk, omega,longines,tissot,fossil,diesel,timex,zegarki, zegary, wymiana baterii, kasperiuk,autoryzowany serwis,skrócenie branzoletki ,naprawa zegarków,dobry zegarmistrz">
    <link rel="stylesheet" type="text/css"
          href="http://www.salon-zegarmistrzowski.pl/tmp/cache/main_stylesheet_1308756503.css" media="all"/>
    <link rel="stylesheet" type="text/css"
          href="http://www.salon-zegarmistrzowski.pl/tmp/cache/news_stylesheet_1308677496.css" media="all"/>
    <link rel="stylesheet" type="text/css"
          href="http://www.salon-zegarmistrzowski.pl/tmp/cache/left_form_stylesheet_1422035877.css" media="all"/>
    <link rel="stylesheet" type="text/css"
          href="http://www.salon-zegarmistrzowski.pl/tmp/cache/menu_stylesheet_1308680513.css" media="all"/>


    <style type="text/css">
        @font-face {
            font-family: My Font;
            src: url('http://www.salon-zegarmistrzowski.pl/uploads/fonts/myfont.eot');
            src: url('http://www.salon-zegarmistrzowski.pl/uploads/fonts/myfont.eot?#iefix') format('embedded-opentype'),
            url('http://www.salon-zegarmistrzowski.pl/uploads/fonts/myfont.ttf') format('truetype');
        }
    </style>


    <script type="text/javascript" src="uploads/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="uploads/js/jqueryslidemenu.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.parentlink a').text('');
            $('.parentlink a').text('');
            $('.prevpage em').text('');
            $('.prevpage a').text('');
            $('.nextpage em').text('');
            $('.nextpage a').text('');
            $('#m002a8fbrp_submit').val('');

        });
    </script>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-25366848-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();

    </script>

</head>
<div id="top_wrapper"></div>
<div id="main_wrapper">
    <div id="top">
        <div id="baner">
            <a href="http://www.facebook.com/profile.php?id=100000985001930" id="facebook"></a>
            <a href="http://www.salon-zegarmistrzowski.pl" id="main_link"></a>
        </div>
    </div>
    <div id="menu">
        <div id="myslidemenu" class="jqueryslidemenu">
            <ul>


                <li><a href="http://www.salon-zegarmistrzowski.pl/" class="link_on">Strona główna</a>


                </li>


                </li>
                <div class="sh_separator"></div>
                <li><a href="http://www.salon-zegarmistrzowski.pl/oferta.html" class="">Oferta</a>


                    <ul>
                        <li><a href="http://www.salon-zegarmistrzowski.pl/oferta/opinie-naszych-klientow.html" class="">opinie
                                klientów</a>


                        </li>
                    </ul>
                </li>
                <div class="sh_separator"></div>
                <li><a href="http://www.salon-zegarmistrzowski.pl/porady.html" class="">Porady</a>


                </li>


                </li>
                <div class="sh_separator"></div>
                <li><a href="http://www.salon-zegarmistrzowski.pl/galeria.html" class="">Galeria</a>


                    <ul>
                        <li><a href="http://www.salon-zegarmistrzowski.pl/galeria/ostatnio-w-naprawie.html" class="">Ostatnio
                                w naprawie :)</a>


                        </li>
                    </ul>
                </li>
                <div class="sh_separator"></div>
                <li><a href="http://www.salon-zegarmistrzowski.pl/linki-do-polecenia.html" class="">zegarmistrz
                        poleca</a>


                    <ul>
                        <li>
                            <a href="http://www.salon-zegarmistrzowski.pl/linki-do-polecenia/polecane-marki-zegarkow.html"
                               class="">Polecane Marki zegarków</a>


                        </li>
                    </ul>
                </li>
                <div class="sh_separator"></div>
                <li><a href="http://www.salon-zegarmistrzowski.pl/promocje.html" class="">Promocje</a>


                </li>


                </li>
                <div class="sh_separator"></div>
                <li><a href="http://www.salon-zegarmistrzowski.pl/kontakt.html" class="">Kontakt</a>


                </li>

            </ul>

        </div>
        <div id="search" class="search_class">

            <form id="cntnt01moduleform_1" method="post" action="http://www.salon-zegarmistrzowski.pl/"
                  class="cms_form">
                <div class="hidden">
                    <input type="hidden" name="mact" value="Search,cntnt01,dosearch,0"/>
                    <input type="hidden" name="cntnt01returnid" value="83"/>
                </div>

                <label for="cntnt01searchinput">Szukaj:&nbsp;</label><input type="text" class="search-input"
                                                                            id="cntnt01searchinput"
                                                                            name="cntnt01searchinput" size="20"
                                                                            maxlength="50"
                                                                            value="wyszukaj na stronie..."
                                                                            onfocus="if(this.value==this.defaultValue) this.value='';"
                                                                            onblur="if(this.value=='') this.value=this.defaultValue;"/>
                <input class="search-button" name="submit" value="Wyślij" type="submit"/>
            </form>

        </div>
    </div>
    <div id="page">
        <div id="left_column">
            <div id="dane_header"></div>
            <div id="dane"></div>
            <div id="news_header"></div>
            <div id="news">
                <p class="news_pagination">
                    Strona&nbsp;1&nbsp;z&nbsp;2
                    &nbsp;<a
                        href="http://www.salon-zegarmistrzowski.pl/index.php?mact=News,m93200,default,1&amp;m93200pagelimit=1&amp;m93200pagenumber=2&amp;m93200returnid=83&amp;page=83">></a>&nbsp;<a
                        href="http://www.salon-zegarmistrzowski.pl/index.php?mact=News,m93200,default,1&amp;m93200pagelimit=1&amp;m93200pagenumber=2&amp;m93200returnid=83&amp;page=83">>></a>
                </p>


                <div class="news_wrapper">
                    <div class="naglowek">

                        Sobota serwis nieczynny

                    </div>
                    <div class="tresc_news">
                        W soboty serwis nieczynny
                    </div>


                    <div class="news_data">
                        24-04-2016
                    </div>

                    <div style="float: right; display: inline;">
                        <a href="http://www.salon-zegarmistrzowski.pl/news/5/83/Sobota-serwis-nieczynny.html"
                           class="news_wiecej">Więcej...</a>
                    </div>

                </div>

            </div>
            <div id="bottom_news">
                <a href="/promocje.html" id="news_link">ZOBACZ WSZYSTKIE PROMOCJE</a>
            </div>
            <div id="form_header"></div>
            <div id="form">

                <script type="text/javascript">
                    function fbht(htid) {
                        var fbhtc = document.getElementById(htid);
                        if (fbhtc) {
                            if (fbhtc.style.display == 'none') {
                                fbhtc.style.display = 'inline';
                            }
                            else {
                                fbhtc.style.display = 'none';
                            }
                        }
                    }
                </script>


                <form id="m002a8moduleform_2" method="post" action="http://www.salon-zegarmistrzowski.pl/"
                      class="cms_form" enctype="multipart/form-data">
                    <div class="hidden">
                        <input type="hidden" name="mact" value="FormBuilder,m002a8,default,1"/>
                        <input type="hidden" name="m002a8returnid" value="83"/>
                        <input type="hidden" name="page" value="83"/>
                        <input type="hidden" name="m002a8fbrp_callcount" value="1"/>
                    </div>

                    <div><input type="hidden" id="m002a8form_id" name="m002a8form_id" value="6"/>
                        <input type="hidden" id="m002a8fbrp_continue" name="m002a8fbrp_continue" value="2"/>
                        <input type="hidden" id="m002a8fbrp_done" name="m002a8fbrp_done" value="1"/>
                    </div>
                    <div class="lewy_formularz">
                        <div class="required form_line"><label for="fbrp__39">Imię*</label><input type="text"
                                                                                                  name="m002a8fbrp__39"
                                                                                                  value="" size="25"
                                                                                                  maxlength="200"
                                                                                                  id="fbrp__39"/>
                        </div>
                        <div class="required form_line"><label for="fbrp__40">E-mail:*</label><input type="text"
                                                                                                     name="m002a8fbrp__40"
                                                                                                     value="" size="25"
                                                                                                     maxlength="80"
                                                                                                     id="fbrp__40"/>
                        </div>
                        <div class="required form_line"><label for="fbrp__41">Treść:*</label><textarea
                                name="m002a8fbrp__41" cols="60" rows="15" class="cms_textarea" id="fbrp__41"></textarea>
                        </div>
                        <div class="form_line captcha">
                            <img alt=""
                                 src="http://www.salon-zegarmistrzowski.pl/modules/Captcha/lib/gd_captcha/captcha.php?sess=CMSSESSID371a9297"/><br>
                            Wpisz tekst z obrazka w to pole<br>(wielkość liter ma znaczenie)<br/><input type="text"
                                                                                                        class="cms_textfield"
                                                                                                        name="m002a8fbrp_captcha_phrase"
                                                                                                        id="m002a8fbrp_captcha_phrase"
                                                                                                        value=""
                                                                                                        size="10"
                                                                                                        maxlength="255"/>
                        </div>
                        <div class="submit"><input class="cms_submit fbsubmit" name="m002a8fbrp_submit"
                                                   id="m002a8fbrp_submit" value="wyślij..." type="submit"/></div>
                    </div>
                </form>


            </div>
            <div id="bottom_form">
                <a href="/kontakt.html" id="form_link">PRZEJDŹ DO ZAKŁADKI KONTAKT</a>
            </div>
        </div>
        <div id="right_column">
            <div id="main_header">Strona główna</div>
            <div id="content">
                <p><img style="display: block; margin-left: auto; margin-right: auto;" src="uploads/images/dsc_0319.jpg"
                        alt="" width="400" height="599"/></p>
                <p></p>
                <p style="text-align: center;"><span style="font-family: comic sans ms,sans-serif;"><strong><span
                                style="font-size: medium;">"Każdy zegarek spełnia swoje zadanie odmierzając czas. Tylko niektóre z nich odmierzają go z emocjami."</span></strong></span>
                </p>
                <p style="text-align: center;"><span style="font-family: comic sans ms,sans-serif;"><strong><span
                                style="font-size: medium;"><br/></span></strong></span></p>
                <p style="text-align: left;"><span style="font-size: medium; color: #ff0000;">Firma istnieje już od 1973r.</span>
                </p>
                <script type="text/javascript">// <![CDATA[
                    (function (d, s, id)(document, 'script', 'facebook-jssdk'));
                    // ]]></script>
            </div>
        </div>
        <div id="nazwy_zegarkow">
            <marquee>
                Bisset - Timemaster - Adriatica - Pierre Ricaud - Grovana - Albatros - Rubicon - Tissot - Certina -
                Citizen - Seiko - Roamer - Tag Heuer - Festina - Atlantic - Delbana - Longines - Esprit - Swatch -
                Fossil - E.Armani - Junghans - Timex - Calvin Klein - Casio - Doxa - Delbana - Q&Q-Lorus - Omega - Rado
                - Claude bernard - Cyma - Maurice Lacroix - Eberhard - Fortis - Ebel - Wostok - Poljot - Edox -
                Frederique Constant - Pobieda - Vector - Lorus - Edox - Diesel
                -Rolex-Cartier-Breitling-Baume&Mercier-Eterna-IWC- Swiss Military-Parnis
                -Xicorr-Parnis-Eta-Ronda-Isa-Miyota-Valjoux i wiele innych

            </marquee>
        </div>
    </div>
</div>
<div id="footer_wrapper">
    <div id="footer">
        <div id="footer_dane">

            <div id="adres">
                ul. Suraska1/lok1, 15-093 Białystok<br/>
            </div>

            <div id="telefon">
                tel.: 693980385
            </div>

            <div id="mail">
                <a href="mailto:salonzegarmistrzowski@vp.pl">salonzegarmistrzowski@vp.pl</a><br/>
                <a href="mailto:rado2004@wp.pl">rado2004@wp.pl</a>
            </div>


        </div>
        <div class="clear">
            <div id="licznik">Liczba odwiedzających: 115782</div>
            <a href="http://extremewebsites.pl" id="copy">Copyright &copy 2011 ExtremeWebsites</div>
    </div>
</div>

<!--[if !IE 8]>

<script type="text/javascript" src="uploads/js/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
    DD_belatedPNG.fix('.prevpage em, .nextpage em, .prevpage a, .nextpage a, .parentlink a, img, #content');
</script>

<![endif]-->

</body>
</html>
