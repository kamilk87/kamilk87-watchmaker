<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * @var $this App\View\AppView
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Watchmaker
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('/bootstrap-3.3.7-dist/css/bootstrap-theme.css'); ?>
    <?= $this->Html->css('/bootstrap-3.3.7-dist/css/bootstrap.css'); ?>
    <?= $this->Html->css('/font-awesome-4.7.0/css/font-awesome.min.css'); ?>
    <?= $this->Html->css('/stylesheets/screen.css'); ?>

    <?= $this->Html->script('/js/jquery-3.1.1.min.js'); ?>
</head>
<body class="container">

<header id="main_header">
    <div class="upper_row">
        <?= $this->fetch('main_header_upper'); ?>
    </div>
    <div class="regular_row">
        <?= $this->element('Header/regular'); ?>
        <?= $this->fetch('main_header_regular'); ?>
    </div>
    <div class="lower_row">
        <?= $this->fetch('main_header_lower'); ?>
        <?= $this->element('Menu/main'); ?>
    </div>
</header>
<section id="upper_section ">
    <div class="upper_row">
        <?= $this->fetch('upper_section_upper'); ?>
    </div>
    <?= $this->Flash->render('flash'); ?>
    <div class="regular_row">
        <?= $this->fetch('upper_section_regular'); ?>
    </div>
    <div class="lower_row">
        <?= $this->fetch('upper_section_lower'); ?>
    </div>
</section>

<section id="content_section">
    <div class="upper_row">
        <div class="">
            <?= $this->fetch('content_section_upper'); ?>
        </div>
    </div>
    <div class="regular_row">
        <?= $this->fetch('content'); ?>
    </div>
    <div class="lower row">
        <div class="blue darken-1" style="height: 8px">
            <?= $this->fetch('content_section_lower'); ?>
        </div>
    </div>
</section>

<section id="lower_section">
    <div class="upper_row">
        <?= $this->fetch('lower_section_upper'); ?>
    </div>
    <div class="regular_row">
        <?= $this->fetch('lower_section_regular'); ?>
    </div>
    <div class="lower_row">
        <?= $this->fetch('lower_section_lower'); ?>
    </div>
</section>

<footer id="main_footer" class="page-footer panel panel-primary">
    <div class="upper_row">
        <?= $this->fetch('main_footer_upper'); ?>
    </div>
    <div class="regular_row row">
        <?= $this->element('Footer/regular'); ?>
        <?= $this->fetch('main_footer_regular'); ?>
    </div>
    <div class="lower_row">
        <?= $this->fetch('main_footer_lower'); ?>
    </div>
</footer>
<?= $this->Html->script('/bootstrap-3.3.7-dist/js/bootstrap.js'); ?>
<?= $this->Html->script('/js/main.js'); ?>
</body>
</html>
