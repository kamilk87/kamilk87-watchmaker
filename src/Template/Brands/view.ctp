<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Edycja marki'), ['action' => 'edit', $brand->id]) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Form->postLink(
                                _('Kasowanie'),
                                ['action' => 'delete', $brand->id],
                                ['confirm' => __('Czy napewno usunąć marke # {0}?', $brand->id)]
                            )
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista Marek'), ['action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista Przedmiotów'), ['controller' => 'Items', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy Przedmiot'), ['controller' => 'Items', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista klientów'), ['controller' => 'Clients', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy klient'), ['controller' => 'Clients', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="brands content">
            <h3><?= h($brand->name) ?></h3>
            <table class="table table-responsive table-bordered">
                <tr>
                    <th scope="row"><?= _('Nazwa') ?></th>
                    <td><?= h($brand->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Numer') ?></th>
                    <td><?= $this->Number->format($brand->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Data utworzenia') ?></th>
                    <td><?= h($brand->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Data ostatniej modyfikacji') ?></th>
                    <td><?= h($brand->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="related">
            <h4><?= _('Powiązane przedmioty') ?></h4>
            <?php if (!empty($brand->items)): ?>
                <table class="table table-responsive table-bordered>">
                    <tr>
                        <th scope="col"><?= _('Numer') ?></th>
                        <th scope="col"><?= _('Nazwa') ?></th>
                        <th scope="col"><?= _('Numer katalogowy') ?></th>
                        <th scope="col"><?= _('Cena') ?></th>
                        <th scope="col"><?= _('Ilość') ?></th>
                        <th scope="col"><?= _('Wartość') ?></th>
                        <th scope="col"><?= _('Marka') ?></th>
                        <th scope="col"><?= _('Szczegóły') ?></th>
                        <th scope="col"><?= _('Data utworzenia') ?></th>
                        <th scope="col"><?= _('Data ostatniej modyfikacji') ?></th>
                        <th scope="col" class="actions"><?= __('Akcje') ?></th>
                    </tr>
                    <?php foreach ($brand->items as $items): ?>
                        <tr>
                            <td><?= h($items->id) ?></td>
                            <td><?= h($items->name) ?></td>
                            <td><?= h($items->item_number) ?></td>
                            <td><?= h(number_format($items->price/100,2)) ?></td>
                            <td><?= h($items->amount) ?></td>
                            <td><?= h($items->alarm) ?></td>
                            <td><?= h($items->brand_id) ?></td>
                            <td><?= h($items->item_attributes) ?></td>
                            <td><?= h($items->created) ?></td>
                            <td><?= h($items->modified) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('Podgląd'), ['controller' => 'Items', 'action' => 'view', $items->id]) ?>
                                <?= $this->Html->link(__('Edycja'), ['controller' => 'Items', 'action' => 'edit', $items->id]) ?>
                                <?= $this->Form->postLink(_('Kasowanie'), ['controller' => 'Items', 'action' => 'delete', $items->id], ['confirm' => __('Czy napewno usunąć przedmiot # {0}?', $items->id)]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>
