<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * @var $this App\View\AppView
 */
?>

<?php $this->start('upper_section_regular'); ?>
<h1 class="row text-center"><?= _('Logowanie / Rejestracja'); ?></h1>
<?php $this->end(); ?>

<div class="row">

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <fieldset class="form-group">
            <legend><?= _('Logowanie'); ?></legend>
            <?= $this->Form->create('User', ['class' => 'form']); ?>
            <?= $this->Form->control('email', ['class' => 'form-control']); ?>
            <?= $this->Form->control('password', ['class' => 'form-control']); ?>
        </fieldset>
        <?= $this->Form->button(_('Logowanie'), ['class' => 'btn btn-primary']); ?>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <fieldset class="form-group">
            <legend><?= _('Rejestracja'); ?></legend>
            <?= $this->Form->create($user, ['url' => '/users/add']); ?>
            <?php
            echo $this->Form->control('username', ['class' => 'form-control']);
            echo $this->Form->control('password', ['class' => 'form-control']);
            echo $this->Form->control('email', ['class' => 'form-control']);
            ?>
        </fieldset>
        <?= $this->Form->button(__('Rejestracja'), ['class' => 'btn btn-primary']); ?>
        <?= $this->Form->end() ?>
    </div>

</div>

