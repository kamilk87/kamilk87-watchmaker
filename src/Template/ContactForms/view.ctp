<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Edycja form kontaktu'), ['action' => 'edit', $contactForm->id]) ?>
                        </td>
                    </tr>
                    <tr>
                        <?= $this->Form->postLink(_('Kasowanie formy kontaktu'), ['action' => 'delete', $contactForm->id], ['confirm' => __('Czy napewno usunąć formę kontaktu # {0}?', $contactForm->id)]) ?>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista form kontaktu'), ['action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowa forma kontaktu'), ['action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Powrót do listy zleceń'), ['controller' => 'Orders', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowe zlecenie'), ['controller' => 'Orders', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista klientów'), ['controller' => 'Clients', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy klient'), ['controller' => 'Clients', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="contactForms view content">
            <h3><?= h($contactForm->name) ?></h3>
            <table class="vertical-table">
                <tr>
                    <th scope="row"><?= _('Nazwa') ?></th>
                    <td><?= h($contactForm->name) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Numer') ?></th>
                    <td><?= $this->Number->format($contactForm->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Data utworzenia') ?></th>
                    <td><?= h($contactForm->created) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= _('Data ostatniej modyfikacji') ?></th>
                    <td><?= h($contactForm->modified) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= _('Powiązani Klienci') ?></h4>
                <?php if (!empty($contactForm->clients)): ?>
                    <table class="table table-responsive table-bordered table-striped">
                        <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id', _('Numer')) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('name', _('Imię')) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('surname', _('Nazwisko')) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('phone', _('Telefon')) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('email', _('Adres email')) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('contact_form_id', _('Preferowana forma kontaktu')) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('created', _('Data utworzenia')) ?></th>
                            <th scope="col" class="actions"><?= _('Akcje') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($contactForm->clients as $client): ?>
                            <tr>
                                <td><?= $this->Number->format($client->id) ?></td>
                                <td><?= h($client->name) ?></td>
                                <td><?= h($client->surname) ?></td>
                                <td><?= h($client->phone) ?></td>
                                <td><?= h($client->email) ?></td>
                                <td><?= $client->has('contact_form') ? $this->Html->link($client->contact_form->name, ['controller' => 'ContactForms', 'action' => 'view', $client->contact_form->id]) : '' ?></td>
                                <td><?= h($client->created) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(_('Podgląd'), ['action' => 'view', $client->id]) ?>
                                    <?= $this->Html->link(_('Edycja'), ['action' => 'edit', $client->id]) ?>
                                    <?= $this->Form->postLink(_('Usuń'), ['action' => 'delete', $client->id], ['confirm' => __('Czy chcesz usunąć klienta # {0}?', $client->id)]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
