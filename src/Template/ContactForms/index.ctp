<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowa forma kontaktu'), ['action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Powrót do listy zleceń'), ['controller' => 'Orders', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowe zlecenie'), ['controller' => 'Orders', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista klientów'), ['controller' => 'Clients', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy klient'), ['controller' => 'Clients', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="contactForms index content">
            <h3><?= __('Contact Forms') ?></h3>
            <table class="table table-responsive table-bordered table-striped">
                <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id', _('Numer')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name', _('Nazwa')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created', _('Data utworzenia')) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('modified', _('Data ostatniej modyfikacji')) ?></th>
                    <th scope="col" class="actions"><?= _('Akcje') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($contactForms as $contactForm): ?>
                    <tr>
                        <td><?= $this->Number->format($contactForm->id) ?></td>
                        <td><?= h($contactForm->name) ?></td>
                        <td><?= h($contactForm->created) ?></td>
                        <td><?= h($contactForm->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(_('Podgląd'), ['action' => 'view', $contactForm->id]) ?>
                            <?= $this->Html->link(_('Edycja'), ['action' => 'edit', $contactForm->id]) ?>
                            <?= $this->Form->postLink(_('Kasowanie'), ['action' => 'delete', $contactForm->id], ['confirm' => __('Czy napewno usunąć formę kontaktu # {0}?', $contactForm->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . _('pierwsza')) ?>
                    <?= $this->Paginator->prev('< ' . _('poprzednia')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(_('następna') . ' >') ?>
                    <?= $this->Paginator->last(_('ostatnia') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => _('Strona {{page}} z {{pages}}, Pokazuje {{current}} wyników z {{count}} wszystkich')]) ?></p>
            </div>
        </div>
    </div>
</div>
