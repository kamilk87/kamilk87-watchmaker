<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <nav class="panel panel-primary" id="actions-sidebar">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?= _('Akcje') ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista form kontaktów'), ['action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Powrót do listy zleceń'), ['controller' => 'Orders', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowe zlecenie'), ['controller' => 'Orders', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Lista klientów'), ['controller' => 'Clients', 'action' => 'index']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?= $this->Html->link(_('Nowy klient'), ['controller' => 'Clients', 'action' => 'add']) ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">

            </div>
        </nav>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="contactForms form content">
            <?= $this->Form->create($contactForm, ['class' => 'form']) ?>
            <fieldset>
                <legend><?= _('Edycja form kontaktu') ?></legend>
                <?php
                echo $this->Form->control('name', [
                    'label' => _('Nazwa formy kontaktu'),
                    'class' => 'form-control',
                    'templates' => [
                        'inputContainer' => '<div class="form-group">{{content}}</div>'
                    ]
                ]);
                ?>
            </fieldset>
            <?= $this->Form->button(_('Zapisz'), ['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
