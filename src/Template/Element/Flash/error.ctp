<section class="flash_section ">
    <div class="alert alert-dismissable alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>
            <?= h($message) ?>
        </strong>
    </div>
</section>
