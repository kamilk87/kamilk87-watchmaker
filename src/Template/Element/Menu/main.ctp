<?php
/**
 * @var $this App\View\AppView
 */
?>
<nav class="navbar navbar-default" id="main_menu">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#mainMenuItemsContainer" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= $this->Url->build('/', true); ?>">
                <!--tutaj cza wstawic ewentualne logo-->
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="mainMenuItemsContainer">
            <ul class="nav navbar-nav">
                <li>
                    <?= $this->Html->link(
                        '<i class="fa fa-home"></i>',
                        [
                            "controller" => "Dashboard",
                            "action" => "index",
                            "plugin" => null
                        ],
                        [
                            'escape' => false
                        ]); ?>
                </li>
                <li>
                    <?= $this->Html->link(
                        __('Zlecenia'),
                        [
                            "controller" => "Orders",
                            "action" => "index",
                            "plugin" => null
                        ],
                        [
                            'escape' => false
                        ]); ?>
                </li>
                <li>
                    <?= $this->Html->link(
                        __('Klienci'),
                        [
                            "controller" => "Clients",
                            "action" => "index",
                            "plugin" => null
                        ],
                        [
                            'escape' => false
                        ]); ?>
                </li>
                <li>
                    <?= $this->Html->link(
                        __('Magazyn'),
                        [
                            "controller" => "Items",
                            "action" => "index",
                            "plugin" => null
                        ],
                        [
                            'escape' => false
                        ]); ?>
                </li>
                <li>
                    <?= $this->Html->link(
                        _('Marki'),
                        [
                            "controller" => "Brands",
                            "action" => "index",
                            "plugin" => null
                        ],
                        [
                            'escape' => false
                        ]); ?>
                </li>
                <?php if (!isset($userObj)): ?>
                    <li>
                        <?= $this->Html->link(
                            __('Zaloguj / Zarejestruj'),
                            [
                                "controller" => "Users",
                                "action" => "login",
                                "plugin" => null
                            ],
                            [
                                'escape' => false
                            ]); ?>
                    </li>
                <?php endif; ?>
                <?php if (isset($userObj)): ?>
                    <li>
                        <?= $this->Html->link(
                            __('Wyloguj'),
                            [
                                "controller" => "Users",
                                "action" => "logout",
                                "plugin" => null
                            ],
                            [
                                'escape' => false
                            ]); ?>
                    </li>
                <?php endif; ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
