<?php

namespace App\Controller;

use App\Controller\AppController;


class DashboardController extends AppController
{

    public function index()
    {
        $firstDayUTS = mktime(0, 0, 0, date("m"), 1, date("Y"));
        $lastMonthRealizedOrders = $this->Orders->find()
            ->where([
                'status_id' => 5,
                'Orders.modified >' => date("Y-m-d H:i:s", $firstDayUTS)
            ])
            ->contain([
                'Clients',
                'Statuses',
                'Priorities'
            ]);

        $this->set('lastMonthRealizedOrders', $lastMonthRealizedOrders);
        $lastClients = $this->Clients->find()
            ->limit(5)
            ->order(['Clients.id' => 'DESC'])
            ->contain([
                'ContactForms'
            ]);
        $lastOrders = $this->Orders->find()
            ->limit(5)
            ->order(['Orders.id' => 'DESC'])
            ->contain([
                'Clients',
                'Statuses',
                'Priorities'
            ]);
        $lastItems = $this->Items->find()
            ->limit(5)
            ->order(['Items.id' => 'DESC'])
            ->contain([
                'Brands'
            ]);

        $this->set([
            'lastClients' => $lastClients,
            'lastOrders' => $lastOrders,
            'lastItems' => $lastItems
        ]);
    }
}
