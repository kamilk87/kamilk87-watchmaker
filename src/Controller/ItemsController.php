<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\ResultSet;

/**
 * Items Controller
 *
 * @property \App\Model\Table\ItemsTable $Items
 */
class ItemsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Brands']
        ];
        /**
         * @var ResultSet $items
         */
        $items = $this->paginate($this->Items);
        $items->each(function($item){
            $item->price /= 100;
        });
        $this->set(compact('items'));
        $this->set('_serialize', ['items']);
    }

    /**
     * View method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $item = $this->Items->get($id, [
            'contain' => ['Brands']
        ]);

        $this->set('item', $item);
        $this->set('_serialize', ['item']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $item = $this->Items->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['price'] = (int)($data['price'] *100);
            $item = $this->Items->patchEntity($item, $data);
            if ($this->Items->save($item)) {
                $this->Flash->success(_('Dane zostały poprawnie zapisane'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(_('Dane nie mogły zostać zapisane, proszę spróbować ponownie'));
        }
        $brands = $this->Items->Brands->find('list', ['limit' => 200]);
        $this->set(compact('item', 'brands'));
        $this->set('_serialize', ['item']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $item = $this->Items->get($id, [
            'contain' => []
        ]);
        $item->price /= 100;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data['price'] = (int)($data['price'] *100);
            $item = $this->Items->patchEntity($item,$data);
            if ($this->Items->save($item)) {
                $this->Flash->success(_('Dane zostały poprawnie zapisane'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(_('Dane nie mogły zostać zapisane, proszę spróbować ponownie'));
        }
        $brands = $this->Items->Brands->find('list', ['limit' => 200]);
        $this->set(compact('item', 'brands'));
        $this->set('_serialize', ['item']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Item id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $item = $this->Items->get($id);
        if ($this->Items->delete($item)) {
            $this->Flash->success(_('Dane zostały poprawnie skasowane'));
        } else {
            $this->Flash->error(_('Dane nie mogły zostać zapisane, proszę spróbować ponownie'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
