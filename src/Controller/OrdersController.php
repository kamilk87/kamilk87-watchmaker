<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Security');
    }

    public function beforeFilter(Event $event)
    {
        $this->Security->setConfig('unlockedActions', ['edit,add']);
        $this->Security->setConfig('unlockedFields', ['files']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Clients', 'Statuses', 'Priorities']
        ];
        $orders = $this->paginate($this->Orders);

        $this->set(compact('orders'));
        $this->set('_serialize', ['orders']);
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['Clients', 'Statuses', 'Priorities', 'Files']
        ]);

        $this->set('order', $order);
        $this->set('_serialize', ['order']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $order = $this->Orders->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['price'] *= 100;
            $order = $this->Orders->patchEntity($order, $data);
            if ($this->Orders->save($order)) {
                $this->Flash->success(_('Dane zostały poprawnie zapisane'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(_('Dane nie mogły zostać zapisane, proszę spróbować ponownie'));
        }
        $clients = $this->Orders->Clients->find('list', ['limit' => 200]);
        $statuses = $this->Orders->Statuses->find('list', ['limit' => 200]);
        $priorities = $this->Orders->Priorities->find('list', ['limit' => 200]);
        $this->set(compact('order', 'clients', 'statuses', 'priorities'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        $order->price /= 100;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data['price'] *= 100;
            if (!isset($data['files']['name'])) {
                unset($data['files']);
            }
            $order = $this->Orders->patchEntity($order, $data);
            if ($this->Orders->save($order)) {
                $this->Flash->success(_('Dane zostały poprawnie zapisane'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(_('Dane nie mogły zostać zapisane, proszę spróbować ponownie'));
        }
        $clients = $this->Orders->Clients->find('list', ['limit' => 200]);
        $statuses = $this->Orders->Statuses->find('list', ['limit' => 200]);
        $priorities = $this->Orders->Priorities->find('list', ['limit' => 200]);
        $this->set(compact('order', 'clients', 'statuses', 'priorities'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(_('Dane zostały poprawnie skasowane'));
        } else {
            $this->Flash->error(_('Dane nie mogły zostać skasowane, proszę spróbować ponownie'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
