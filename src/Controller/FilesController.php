<?php

namespace App\Controller;

use App\Controller\AppController;
use Proffer\Lib\ProfferPath;
use Cake\Event\Event;

/**
 * Files Controller
 *
 * @property \App\Model\Table\FilesTable $Files
 */
class FilesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Security');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->eventManager()->off($this->Csrf);
        $this->Security->setConfig('unlockedActions', ['delete']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $files = $this->paginate($this->Files);

        $this->set(compact('files'));
        $this->set('_serialize', ['files']);
    }

    /**
     * View method
     *
     * @param string|null $id File id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $file = $this->Files->get($id, [
            'contain' => []
        ]);

        $this->set('file', $file);
        $this->set('_serialize', ['file']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $file = $this->Files->newEntity();
        if ($this->request->is('post')) {
            $file = $this->Files->patchEntity($file, $this->request->getData());
            if ($this->Files->save($file)) {
                $this->Flash->success(__('The file has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The file could not be saved. Please, try again.'));
        }
        $this->set(compact('file'));
        $this->set('_serialize', ['file']);
    }

    /**
     * Edit method
     *
     * @param string|null $id File id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $file = $this->Files->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $file = $this->Files->patchEntity($file, $this->request->getData());
            if ($this->Files->save($file)) {
                $this->Flash->success(__('The file has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The file could not be saved. Please, try again.'));
        }
        $this->set(compact('file'));
        $this->set('_serialize', ['file']);
    }

    /**
     * Delete method
     *
     * @param string|null $id File id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if (!$id) {
            $id = $this->request->getData('id');
        }
        $this->request->allowMethod(['post', 'delete']);
        $file = $this->Files->get($id);
        $path = new ProfferPath($this->Files, $file, 'name', ['dir' => $file->dir]);
        $path->deleteFiles($path->getFolder());
        $deletingSuccessfully = $this->Files->delete($file);
        if (!$this->request->is('ajax')) {
            if ($deletingSuccessfully) {
                $this->Flash->success(__('Dane zostały usuniete!'));
            } else {
                $this->Flash->error(__('Dane nie zostały usuniete!'));
            }
            return $this->redirect(['action' => 'index']);
        }
        if ($deletingSuccessfully) {
            $this->set('message', __('Dane zostały usuniete!'));
        } else {
            $this->set('message', __('Dane nie zostały usuniete!'));
        }
        $this->set('_serialize', ['message']);
    }
}
