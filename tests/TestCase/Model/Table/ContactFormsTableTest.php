<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContactFormsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContactFormsTable Test Case
 */
class ContactFormsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContactFormsTable
     */
    public $ContactForms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contact_forms',
        'app.clients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ContactForms') ? [] : ['className' => 'App\Model\Table\ContactFormsTable'];
        $this->ContactForms = TableRegistry::get('ContactForms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContactForms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
